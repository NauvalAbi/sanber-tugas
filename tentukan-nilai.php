<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php

function tentukan_nilai($number)
{
    if($number >= 85){
        echo "Nilai anda $number Sangat Baik" . "<br>";
    }else if($number >= 70){
        echo "Nilai anda $number Baik" . "<br>";
    }else if($number >= 60){
        echo "Nilai anda $number Cukup" . "<br>";
    }else{
        echo "Nilai anda $number Kurang" . "<br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang

?>
    
</body>
</html>