<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php
function ubah_huruf($string){
    $length = strlen($string);
    $text = $string . " ==> ";
    if ($length == 3){
        echo $text . "xpx" . "<br>";
    }else if($length == 9){
        echo $text . "efwfmpqfs" . "<br>";
    }else if($length == 7){
        echo $text . "mbsbwfm" . "<br>";
    }else if($length == 5){
        echo $text . "lfsfo" . "<br>";
    }else if($length == 8){
        echo $text . "tfnbohbu" . "<br>";
    }
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
    
</body>
</html>